using System;
using System.IO;

public class word_mangle
{
   public static void Main(string[] args)
   {
       // Enter your code here
       using(StreamReader reader = new StreamReader(args[0]))
       {
           string line = reader.ReadLine();
           ReverseWords(line);
           ReverseOrder(line);
       }
   }
   
   private static void ReverseOrder(string line)
   {
       ReverseWords(line, false);   
   }
   
   private static void ReverseWords(string line)
   {
       ReverseWords(line, true);
   }
   
   private static void ReverseWords(string line, bool shouldReverseWholeWord)
   {
       var lineArray = line.ToCharArray();
       if(shouldReverseWholeWord)
           ReverseWord(lineArray, 0, lineArray.Length - 1);
       
       int j = 0;
       for(int i = 0; i < lineArray.Length; i++)
       {
           if(lineArray[i] == ' ' || i == lineArray.Length - 1)
           {
               if(lineArray.Length - 1 == i)
                   ReverseWord(lineArray, j, i);
               else    
                   ReverseWord(lineArray, j, i - 1);
                   
               j = i+1; 
           }
       }
       
       Console.WriteLine(lineArray);
   }
   
   private static void ReverseWord(char[] word, int startIndex, int endIndex)
   {
       char temp;
       int j = endIndex;
       for(int i = startIndex; i < j; i++, j--)
       {
           temp = word[j];
           word[j] = word[i];
           word[i] = temp;
       }   
   }
}
