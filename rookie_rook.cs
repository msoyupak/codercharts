using System;
using System.IO;
using System.Collections.Generic;

public class rookie_rook
{
   public static void Main(string[] args)
   {
       using(var reader = new StreamReader(args[0]))
       {
           var numberOfCases = int.Parse(reader.ReadLine());
           for(int i=0; i<numberOfCases; i++)
           {
               var rows = new List<int>();
               var cols = new List<int>();
               
               var line = reader.ReadLine();
               var nums = line.Split(' ');
               var d = int.Parse(nums[0]);
               var r = int.Parse(nums[1]);
               
               for(int j=0; j<r; j++)
               {
                   line = reader.ReadLine();
                   nums = line.Split(' ');
                   var x = int.Parse(nums[0]);
                   var y = int.Parse(nums[1]);
                   
                   if(!rows.Contains(x))
                       rows.Add(x);
                   if(!cols.Contains(y))
                       cols.Add(y);
               }
               
               Console.WriteLine((rows.Count * d) + (cols.Count * (d - rows.Count)));
           }
       }              
   }
}

