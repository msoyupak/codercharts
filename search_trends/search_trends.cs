using System;
using System.IO;
using System.Collections.Generic;
using System.Collections;
using System.Linq;

public class search_trends
{
   public static void Main(string[] args)
   {
       var dict = new Dictionary<string,int>();

       using(var reader = new StreamReader(args[0]))
       {
           while(true)
           {
               var line = reader.ReadLine();
               if(string.IsNullOrEmpty(line))
                   break;
                   
               if(dict.ContainsKey(line))
               {
                   dict[line] = dict[line] + 1;
               }
               else
               {
                   dict.Add(line, 1);
               }
           }
       }
       
       List<KeyValuePair<string, int>> dictList = dict.ToList();
       dictList.Sort((first, second) =>
       {
           return second.Value.CompareTo(first.Value);
       }
       );
       
       for(int i=0; i<10 && i<dictList.Count(); i++)
       {
           Console.WriteLine("{0} {1}", dictList[i].Value, dictList[i].Key);
       }
   }
}
