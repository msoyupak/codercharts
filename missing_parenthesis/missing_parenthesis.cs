using System;
using System.IO;

public class missing_parenthesis
{
   public static void Main(string[] args)
   {
       using(var reader = new StreamReader(args[0]))
       {
           var numLines = int.Parse(reader.ReadLine());
           for(int i=0; i<numLines; i++)
           {
               var elements = reader.ReadLine().Split(' ');
               var a = int.Parse(elements[0]);
               var op1 = elements[1];
               var b = int.Parse(elements[2]);
               var op2 = elements[3];
               var c = int.Parse(elements[4]);
               
               var first = Calculate(Calculate(a, op1, b), op2, c);
               var second = Calculate(a, op1, Calculate(b, op2, c));
               
               if(first >= second)
                   Console.WriteLine(first);
               else
                   Console.WriteLine(second);
               
           }
       }
   }
   
   private static int Calculate(int a, string op, int b)
   {
       switch(op)
       {
           case "+":
               return a+b;
           case "-":
               return a-b;
           case "*":
               return a*b;
           case "/":
               return a/b;
           default:
               return 0;
       }
   }
}
