using System;
using System.IO;

public class fast_grep
{
   public static void Main(string[] args)
   {
       var word = args[0];
       var path = args[1];
       
       using(var reader = new StreamReader(path))
       {
           int lineNumber = 0;
           while(reader.Peek() > -1)
           {
               lineNumber++;
               var line = reader.ReadLine();
               if(line.Contains(word))
               {
                   Console.WriteLine(lineNumber);
                   return;
               }
          }
          
          Console.WriteLine(0);
       }
   }
}
