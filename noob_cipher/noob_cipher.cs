using System;
using System.IO;
using System.Collections.Generic;

public class noob_cipher
{
   public static void Main(string[] args)
   {
       // Enter your code here
       using(var reader = new StreamReader(args[1]))
       {
           var substitutionTable = args[0].ToCharArray();
           var dict = new Dictionary<char, char>();
           var currentChar = 'A';
           foreach(var c in substitutionTable)
           {
               dict.Add(c, currentChar++);
           }
           
           foreach(var c in reader.ReadToEnd().ToCharArray())
           {
               if(c == ' ')
               {
                   Console.Write(' ');
                   continue;
               }
               
               if(c == '\n')
                   continue;
                   
               Console.Write(dict[c]);
           }
       }
           
   }
}

