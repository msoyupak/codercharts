using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

public class vote_count
{
   public static void Main(string[] args)
   {
       if(args == null || args.Length != 1)
           throw new ArgumentException();
           
       using(StreamReader sr = new StreamReader(args[0]))
       {
           if (sr.Peek() < 0) 
           {
               throw new ArgumentException();
           }
           
           int numChoices = int.Parse(sr.ReadLine());
           Dictionary<string, int> votes = new Dictionary<string, int>();
           
           for(int i = 0; i< numChoices; i++)
           {
               votes.Add(sr.ReadLine(), 0);
           }
           
           int numRows = int.Parse(sr.ReadLine());
           int max = -1;
           int total = 0;
           
           for(int i = 0; i < numRows; i++)
           {
               string row = sr.ReadLine();
               string choice = row.Split(' ')[0];
               
               if(!votes.ContainsKey(choice))
                   continue;
                   
               int vote = int.Parse(row.Split(' ')[1]);
               total += vote;
               votes[choice]+=vote;
               
               if(votes[choice] > max)
                   max = votes[choice];
           }
           
           List<string> list = new List<string> (votes.Keys);
	   list.Sort();
           
           foreach(string choice in list)
           {
               if(votes[choice] == max)
               {
                   double result = (((double)votes[choice] * 100)/total);
                   Console.WriteLine("{0} {1}", choice, result.ToString("N2", CultureInfo.InvariantCulture));
               }
           }
       }
   }
}

