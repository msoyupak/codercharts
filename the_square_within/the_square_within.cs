using System.IO;
using System;

public class square_within
{
   public static void Main(string[] args)
   {
       // Enter your code here
       using(var reader = new StreamReader(args[0]))
       {
           while(true)
           {
               var line = reader.ReadLine();
               if(string.IsNullOrEmpty(line))
                   break;
               var num = int.Parse(line);
               
               if(num < 1)
                   Console.WriteLine(0);
               else    
                   Console.WriteLine((num * (2*num +1) * (num+1))/6);
            }
       }    
   }
}
